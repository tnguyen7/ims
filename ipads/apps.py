from django.apps import AppConfig


class IpadsConfig(AppConfig):
    name = 'ipads'
