from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.views.generic.edit import UpdateView
from django.urls import resolve

from .models import Ipad
from .forms import IpadForm


def index(request):
    return render(request, 'ipads/index.html')


def ipads_by_school(request, school_id):
    #school_ipads = Ipad.objects.filter(asset_number__icontains=school_id).order_by('asset_number')
    school_ipads = Ipad.objects.filter(
        school_id__iexact=school_id).order_by('asset_number')

    if school_ipads:
        # teachers = Ipad.objects.order_by('teacher_name').distinct('teacher_name')
        teachers = school_ipads.order_by(
            'teacher_name').distinct('teacher_name')

        school_id = school_id

        count_working = get_ipad_counts(0, school_id)
        count_in_repair = get_ipad_counts(1, school_id)
        count_lost = get_ipad_counts(2, school_id)

        context = {
            'school_ipads': school_ipads,
            'teachers': teachers,
            'school_id': school_id,
            'count_working': count_working,
            'count_in_repair': count_in_repair,
            'count_lost': count_lost
        }

        return render(request, 'ipads/ipads_by_school.html', context)
    else:
        raise Http404("School does not exist.")


def ipads_by_teacher(request, school_id, teacher_name):
    school_ipads = Ipad.objects.filter(
        school_id__iexact=school_id, teacher_name__iexact=teacher_name)
    teachers = Ipad.objects.filter(
        school_id__iexact=school_id).distinct('teacher_name')

    count_working = get_ipad_counts(0, school_id)
    count_in_repair = get_ipad_counts(1, school_id)
    count_lost = get_ipad_counts(2, school_id)

    # teachers = Ipad.objects.order_by('teacher_name').distinct('teacher_name')
    # teachers = school_ipads.order_by('teacher_name').distinct('teacher_name')

    context = {
        'school_ipads': school_ipads,
        'teachers': teachers,
        'school_id': school_id,
        'count_working': count_working,
        'count_in_repair': count_in_repair,
        'count_lost': count_lost
    }

    return render(request, 'ipads/ipads_by_school.html', context)


def unassigned_ipads(request, school_id):
    school_ipads = Ipad.objects.filter(
        school_id__iexact=school_id, teacher_name='')
    # school_ipads = school_ipads.filter(
    #     teacher_name='').order_by('asset_number')

    # teachers = Ipad.objects.order_by('teacher_name').distinct('teacher_name')
    teachers = Ipad.objects.filter(
        school_id__iexact=school_id).distinct('teacher_name')

    count_working = get_ipad_counts(0, school_id)
    count_in_repair = get_ipad_counts(1, school_id)
    count_lost = get_ipad_counts(2, school_id)

    context = {
        'school_ipads': school_ipads,
        'teachers': teachers,
        'school_id': school_id,
        'count_working': count_working,
        'count_in_repair': count_in_repair,
        'count_lost': count_lost
    }

    return render(request, 'ipads/ipads_by_school.html', context)


def get_ipad_counts(flag, school_id):
    if flag == 0:
        count = Ipad.objects.filter(
            school_id__iexact=school_id, status='Working').count()
    elif flag == 1:
        count = Ipad.objects.filter(
            school_id__iexact=school_id, status='In Repair').count()
    elif flag == 2:
        count = Ipad.objects.filter(
            school_id__iexact=school_id, status='Lost').count()
    return count


def in_repair(request, asset_number):
    ipad = Ipad.objects.get(asset_number=asset_number)
    ipad.status = 'In Repair'
    ipad.save()

    # context = {
    #     'ipad': ipad
    # }

    # return render(request, 'ipads/ipads_by_school.html')
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


def repaired(request, asset_number):
    ipad = Ipad.objects.get(asset_number=asset_number)
    ipad.status = 'Working'
    ipad.save()

    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


def lost(request, asset_number):
    ipad = Ipad.objects.get(asset_number=asset_number)
    ipad.status = 'Lost'
    ipad.save()

    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


def found(request, asset_number):
    ipad = Ipad.objects.get(asset_number=asset_number)
    ipad.status = 'Working'
    ipad.save()

    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


def update_ipad(request, school_id, asset_number):
    if request.method == 'POST':

        ipad = Ipad.objects.get(asset_number=asset_number)

        ipad.serial_number = request.POST['serial_number']
        ipad.teacher_name = request.POST['teacher_name']
        ipad.room_number = request.POST['room_number']
        ipad.label = request.POST['custom_label']
        ipad.school_id = request.POST['school_id']

        ipad.save()

        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


def edit_ipad_old(request, asset_number):
    ipad = get_object_or_404(Ipad, asset_number=asset_number)
    if request.method == "POST":
        form = IpadForm(request.POST, instance=ipad)
        if form.is_valid():
            ipad = form.save(commit=False)

            ipad.save()
            # print(request.resolver_match.url_name)
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
            # return HttpResponseRedirect(request.POST.get('full_path'))

            # next = request.POST.get('next', '/')
            # return HttpResponseRedirect(next)
    else:
        form = IpadForm(instance=ipad)

    return render(request, 'ipads/edit_ipad.html', {'form': form})
