from django.db import models


class Ipad(models.Model):
    asset_number = models.CharField(max_length=20, primary_key=True)
    serial_number = models.CharField(max_length=30, blank=True)
    school_id = models.CharField(max_length=5, blank=True)
    teacher_name = models.CharField(max_length=50, blank=True)
    room_number = models.CharField(max_length=10, blank=True)
    status = models.CharField(max_length=20, default='Working')
    label = models.CharField(max_length=30, blank=True)

    def __str__(self):
        return self.asset_number
