from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('<str:school_id>', views.ipads_by_school, name='ipads_by_school'),
    path('<str:school_id>/unassigned',
         views.unassigned_ipads, name='unassigned_ipads'),
    path('in_repair/<str:asset_number>', views.in_repair, name='in_repair'),
    path('repaired/<str:asset_number>', views.repaired, name='repaired'),
    path('found/<str:asset_number>', views.found, name='found'),
    path('lost/<str:asset_number>', views.lost, name='lost'),
    path('<str:school_id>/<str:teacher_name>',
         views.ipads_by_teacher, name='ipads_by_teacher'),
    path('<str:school_id>/update_ipad/<str:asset_number>',
         views.update_ipad, name='update_ipad')
]
