from django import forms
from .models import Ipad


class IpadForm(forms.ModelForm):
    class Meta:
        model = Ipad
        fields = ('asset_number', 'serial_number', 'school_id',
                  'teacher_name', 'room_number', 'label')
        widgets = {
            'asset_number': forms.TextInput(attrs={'readonly': 'readonly'}),
        }
