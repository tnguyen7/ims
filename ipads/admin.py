from django.contrib import admin
from .models import Ipad
from import_export.admin import ImportMixin
from import_export import resources


class IpadResource(resources.ModelResource):

    class Meta:
        model = Ipad
        import_id_fields = ('asset_number',)


# admin.site.register(Ipad)

@admin.register(Ipad)
class IpadAdmin(ImportMixin, admin.ModelAdmin):
    list_display = ('asset_number', 'serial_number', 'school_id',
                    'teacher_name', 'room_number', 'status', 'label')
    search_fields = ["asset_number", "serial_number", "school_id",
                     "teacher_name", "room_number", "status", "label"]
    resource_class = IpadResource
